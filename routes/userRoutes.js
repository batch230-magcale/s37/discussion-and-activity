const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

router.post("/register", (request, response) => {
	userController.registeredUser(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})

/*
	function loginUser(reqBody){
	
	}

	loginUser(request.body)
*/

// --------------s38 Activity--------------------

/*router.post("/details", (request, response) => {
	userController.getProfileA(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/details", (request, response) => {
	userController.getProfileB(request.body).then(resultFromController => response.send(resultFromController));
})*/


router.get("/details", auth.verify, userController.getProfile);

router.post("/enroll", auth.verify, userController.enroll);


module.exports = router;