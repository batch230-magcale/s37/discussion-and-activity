const mongoose = require("mongoose");
const Course = require("../models/courses");

// Function for adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course.
// NOTE: include screenshot of successful admin addCourse and  screenshot of not successful addCourse by a user that is not admin.
module.exports.addCourse = (reqBody, result) => {
	if(result.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			slots: reqBody.slots
		})
		return newCourse.save().then((newCourse, error) => 
		{
			if(error){
				return error;
			}
			else{
				return newCourse;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

// GET all course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// GET all Active Courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// GET specific course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// [Additional Example]
// [Arrow function to regular function]
// Get ALL course feature converted to regular function
// 'GetSpecificCourseInRegularFunction' acts as a variable/storage of getCourseV2() function so it could be exported
module.exports.GetSpecificCourseInRegularFunction = 
function getCourseV2(courseId){
	return Course.findById(courseId).
	then(
		function sendResult(result){
			return result;
		}
	);
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		// Promise.resolve is required if it expects a solved promise
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	// Compared to regular return it will not work
	// return 'User must be ADMIN to access this functionality';
}

// ----------- s40 Activity -----------

module.exports.updateCourseIfActive = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: newData.course.isActive
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return result;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}

